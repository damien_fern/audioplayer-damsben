package com.example.a5irc_audioplayerdamsben;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.databinding.DataBindingUtil;
import android.os.IBinder;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.a5irc_audioplayerdamsben.databinding.ActivityMainBinding;
import com.example.a5irc_audioplayerdamsben.model.AudioFile;
import com.example.a5irc_audioplayerdamsben.model.AudioFileListFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnRequestPermissionsResultCallback {

    private ActivityMainBinding binding;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 0;
    private PlayerService mService;
    private PlayerFragment playerFragment;
    private AudioFileListFragment listFragment;
    private List<AudioFile> listAudioFiles;
    private boolean mBound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        showStartup();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, PlayerService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(connection);
        mBound = false;
    }

    public void showStartup() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        listFragment = new AudioFileListFragment();
        listAudioFiles = getAllAudioFiles();
        listFragment.setAudioFileList(listAudioFiles);
        playerFragment = new PlayerFragment();
        transaction.replace(R.id.fragment_container, listFragment);
        transaction.replace(R.id.bottom_bar_container, playerFragment);

        transaction.commit();
    }

    private void askPermission()
    {
        if (!isPermissionOK())
        {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        }

        System.out.println("test permission");
    }

    private boolean isPermissionOK()
    {
        return ContextCompat.checkSelfPermission(getApplicationContext(),Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        System.out.println("Done"); // TODO : NE PASSE JAMAIS ICI
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private List<AudioFile> getAllAudioFiles()
    {
        List<AudioFile> audioFiles = new ArrayList<>();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.YEAR
        };
        askPermission();
        while (!isPermissionOK()) { // Sale

        }
        Cursor cursor = getApplicationContext().getContentResolver().query(uri, projection,null,null,null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            AudioFile audioFile = cursorToAudioFile(cursor);
            audioFiles.add(audioFile);
            cursor.moveToNext();
        }
        cursor.close();
        return audioFiles;
    }

    private AudioFile cursorToAudioFile(Cursor cursor) {
        AudioFile oneAudioFile = new AudioFile();
        oneAudioFile.setFilePath(cursor.getString(0));
        oneAudioFile.setAlbum(cursor.getString(1));
        oneAudioFile.setArtist(cursor.getString(2));
        oneAudioFile.setDuration(cursor.getInt(3));
        oneAudioFile.setTitle(cursor.getString(4));
        oneAudioFile.setYear(cursor.getInt(5));

        return oneAudioFile;
    }

    private ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            PlayerService.PlayerBinder binder = (PlayerService.PlayerBinder) service;
            mService = binder.getService();
            mService.setListAudioFiles(listAudioFiles);
            playerFragment.setMediaPlayer(mService);
            listFragment.setListListener(mService);
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
}
