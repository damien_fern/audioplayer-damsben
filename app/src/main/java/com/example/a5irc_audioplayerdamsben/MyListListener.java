package com.example.a5irc_audioplayerdamsben;

import android.net.Uri;

import com.example.a5irc_audioplayerdamsben.model.AudioFile;

import java.io.IOException;

public interface MyListListener {
    void play(AudioFile audioFile) throws IOException;
}
