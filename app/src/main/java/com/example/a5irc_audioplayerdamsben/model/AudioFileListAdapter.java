package com.example.a5irc_audioplayerdamsben.model;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a5irc_audioplayerdamsben.MyListListener;
import com.example.a5irc_audioplayerdamsben.R;
import com.example.a5irc_audioplayerdamsben.databinding.AudioFileItemBinding;
import com.example.a5irc_audioplayerdamsben.viewModel.AudioFileViewModel;

import java.io.IOException;
import java.util.List;


public class AudioFileListAdapter extends
        RecyclerView.Adapter<AudioFileListAdapter.ViewHolder> {
    List<AudioFile> audioFileList;
    MyListListener listListener;

    public AudioFileListAdapter(List<AudioFile> fileList) {
        assert fileList != null;
        audioFileList = fileList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AudioFileItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.audio_file_item, parent,false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        AudioFile file = audioFileList.get(position);
        holder.viewModel.setAudioFile(file);
    }

    public void setListListener(MyListListener listListener) {
        this.listListener = listListener;
    }

    @Override
    public int getItemCount() {
        return audioFileList.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder {
        private AudioFileItemBinding binding;
        private AudioFileViewModel viewModel = new AudioFileViewModel();

        ViewHolder(final AudioFileItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        listListener.play(binding.getAudioFileViewModel().getAudioFile());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            this.binding.setAudioFileViewModel(viewModel);
        }
    }


}
