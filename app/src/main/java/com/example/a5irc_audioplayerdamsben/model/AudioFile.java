package com.example.a5irc_audioplayerdamsben.model;

import android.net.Uri;

import java.util.Locale;

public class AudioFile {
    private String title;
    private Uri filePath;
    private String artist;
    private String album;
    private String genre;
    private int year;
    private int duration;

    public AudioFile(String title, String artist, String album, int duration) {
        this.title = title;
        this.artist = artist;
        this.album = album;
        this.duration = duration;
    }

    public AudioFile() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Uri getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = Uri.parse(filePath);
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getDurationText() {
        int second = duration % 60;
        int durationMinute = (duration - second) / 60;
        int minute = durationMinute % 60;
        int hour = (durationMinute - minute) / 60;
        if(hour > 0)
            return String.format(Locale.getDefault(),
                    "%02d:%02d:%02d",hour,minute,second);
        return String.format(Locale.getDefault(),
                "%02d:%02d",minute,second);
    }
}
