package com.example.a5irc_audioplayerdamsben.model;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a5irc_audioplayerdamsben.MyListListener;
import com.example.a5irc_audioplayerdamsben.R;
import com.example.a5irc_audioplayerdamsben.databinding.AudioFileListFragmentBinding;

import java.util.ArrayList;
import java.util.List;

public class AudioFileListFragment extends Fragment {
    List<AudioFile> audioFileList = new ArrayList<>();
    private MyListListener listListener;
    private AudioFileListAdapter adapter;

    public void setAudioFileList(List<AudioFile> audioFileList) {
        this.audioFileList = audioFileList;
    }

    public List<AudioFile> getAudioFileList() {
        return audioFileList;
    }

    public AudioFileListFragment()
    {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        AudioFileListFragmentBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.audio_file_list_fragment,container,false);
        binding.audioFileList.setLayoutManager(new LinearLayoutManager(
                binding.getRoot().getContext()));
        adapter = new AudioFileListAdapter(audioFileList);
        binding.audioFileList.setAdapter(adapter);

        return binding.getRoot();

    }


    public void setListListener(MyListListener listener) {
        listListener = listener;
        adapter.setListListener(listListener);
    }
}
