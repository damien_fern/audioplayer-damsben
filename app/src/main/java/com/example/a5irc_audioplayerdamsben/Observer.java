package com.example.a5irc_audioplayerdamsben;

import com.example.a5irc_audioplayerdamsben.model.AudioFile;

public interface Observer {
    public void update(final AudioFile music);
}