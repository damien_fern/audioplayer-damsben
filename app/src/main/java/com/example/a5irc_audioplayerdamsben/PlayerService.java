package com.example.a5irc_audioplayerdamsben;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.example.a5irc_audioplayerdamsben.model.AudioFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PlayerService extends Service implements PlayerListener, MyListListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener {

    private final Binder binder = new PlayerBinder();
    private AudioFile currentSong = new AudioFile("","","",0);
    private MediaPlayer player;
    private List<Observer> observers = new ArrayList<>();
    private List<AudioFile> listAudioFiles;

    @Override
    public void onCreate() {
        super.onCreate();
        player = new MediaPlayer();
        player.setOnCompletionListener(this);
        player.setOnPreparedListener(this);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY_COMPATIBILITY;
    }

    public void play(AudioFile audioFile) throws IOException {
        player.reset();
        currentSong = audioFile;
        player.setDataSource(getApplicationContext(), currentSong.getFilePath());
        player.prepare();
    }

    public void stop() {
    }

    public boolean isPlaying() {
        return player.isPlaying();
    }

    @Override
    public void togglePlayPause() {
        if (currentSong != null) {
            if (player.isPlaying()) {
                player.pause();
            } else {
                player.start();
            }
        }
    }

    @Override
    public void previous() {
        AudioFile previous = (listAudioFiles.indexOf(currentSong) - 1) < 0 ?
                currentSong
                : listAudioFiles.get(listAudioFiles.indexOf(currentSong) - 1);
        try {
            play(previous);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void next() {
        AudioFile next = (listAudioFiles.indexOf(currentSong) + 1) >= listAudioFiles.size() ?
                listAudioFiles.get(0)
                : listAudioFiles.get(listAudioFiles.indexOf(currentSong) + 1);
        try {
            play(next);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public AudioFile getCurrentSong() {
        return currentSong;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        player.start();
        notifyObservers();
    }

    @Override
    public void register(final Observer observer) {
        if (!observers.contains(observer)) {
            observers.add(observer);
        }
    }

    @Override
    public void unregister(final Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (final Observer observer : observers) {
            observer.update(currentSong);
        }
    }

    public void setListAudioFiles(List<AudioFile> listAudioFiles) {
        this.listAudioFiles = listAudioFiles;
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        this.next();
    }

    class PlayerBinder extends Binder {
        public PlayerService getService() {
            return PlayerService.this;
        }
    }

}
