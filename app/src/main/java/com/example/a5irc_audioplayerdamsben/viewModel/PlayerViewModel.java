package com.example.a5irc_audioplayerdamsben.viewModel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.net.Uri;

import com.example.a5irc_audioplayerdamsben.Observer;
import com.example.a5irc_audioplayerdamsben.PlayerListener;
import com.example.a5irc_audioplayerdamsben.R;
import com.example.a5irc_audioplayerdamsben.model.AudioFile;

import java.io.IOException;

public class PlayerViewModel extends BaseObservable implements Observer {

    private AudioFile currentSong = new AudioFile("","","",0);
    private int play_pause_button = R.drawable.play_button;
    private PlayerListener mediaPlayer;

    public void setMediaPlayer(PlayerListener listener)
    {
        mediaPlayer = listener;
    }

    private void setCurrentSong(AudioFile file)
    {
        currentSong = file;
        notifyChange();
    }

    public void changeSong(AudioFile file)
    {
//        try {
//            mediaPlayer.play(file);
//            setCurrentSong(file);
//            checkPlayerState();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    @Bindable
    public String getArtist()
    {
        if (!isMediaPlayerInitialize()) {
            return "";
        }
        return currentSong.getArtist();
    }

    @Bindable
    public String getTitle()
    {
        if (!isMediaPlayerInitialize()) {
            return "";
        }
        return currentSong.getTitle();
    }

    private boolean isMediaPlayerInitialize()
    {
        return mediaPlayer != null;
    }
    @Bindable
    public int getPlay_pause_button()
    {
        return play_pause_button;
    }

    private void setPlay_pause_button(int play_pause_button)
    {
        this.play_pause_button = play_pause_button;
        notifyPropertyChanged(com.example.a5irc_audioplayerdamsben.BR.play_pause_button);
    }

    public void onPlayClick()
    {
        mediaPlayer.togglePlayPause();
        checkPlayerState();
    }

    public void checkPlayerState()
    {
        if (mediaPlayer.isPlaying()) {
            setPlay_pause_button(R.drawable.pause_button);
        } else {
            setPlay_pause_button(R.drawable.play_button);
        }
    }

    public void onPreviousClick()
    {
        mediaPlayer.previous();
    }

    public void onNextClick()
    {
        mediaPlayer.next();
    }

    @Override
    public void update(AudioFile music) {
        setCurrentSong(music);
        checkPlayerState();
    }
}
