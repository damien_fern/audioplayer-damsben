package com.example.a5irc_audioplayerdamsben.viewModel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.example.a5irc_audioplayerdamsben.model.AudioFile;

public class AudioFileViewModel extends BaseObservable {
    private AudioFile audioFile = new AudioFile();
    public void setAudioFile(AudioFile file) {
        audioFile = file;
        notifyChange();
    }
    @Bindable
    public String getArtist() {
        return audioFile.getArtist();
    }
    @Bindable
    public String getTitle() {
        return audioFile.getTitle();
    }
    @Bindable
    public String getAlbum() {
        return audioFile.getAlbum();
    }
    @Bindable
    public String getDuration() {
        return audioFile.getDurationText();
    }

    public AudioFile getAudioFile() {
        return audioFile;
    }
}
