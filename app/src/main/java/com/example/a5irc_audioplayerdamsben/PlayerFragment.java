package com.example.a5irc_audioplayerdamsben;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a5irc_audioplayerdamsben.databinding.BarPlayerFragmentBinding;
import com.example.a5irc_audioplayerdamsben.model.AudioFile;
import com.example.a5irc_audioplayerdamsben.viewModel.PlayerViewModel;

import java.util.Observable;

public class PlayerFragment extends Fragment {

    private AudioFile music;
    private PlayerListener mediaPlayer;
    private PlayerViewModel viewModel;

    public void setMediaPlayer(PlayerListener listener) {
        mediaPlayer = listener;
        viewModel.setMediaPlayer(listener);
        mediaPlayer.register(viewModel);
    }

    public void setMusic(AudioFile music) {
        this.music = music;
        viewModel.changeSong(music);
    }

    public PlayerFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        BarPlayerFragmentBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.bar_player_fragment, container, false);
        viewModel = new PlayerViewModel();
        viewModel.setMediaPlayer(mediaPlayer);
        binding.setPlayerViewModel(viewModel);
        return binding.getRoot();
    }
}
