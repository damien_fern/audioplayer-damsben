package com.example.a5irc_audioplayerdamsben;

import android.net.Uri;

import com.example.a5irc_audioplayerdamsben.model.AudioFile;
import com.example.a5irc_audioplayerdamsben.viewModel.PlayerViewModel;

import java.io.IOException;

public interface PlayerListener extends Subject {
    void play(AudioFile audioFile) throws IOException;
    boolean isPlaying();
    void togglePlayPause();
    void previous();
    void next();

    AudioFile getCurrentSong();
}
